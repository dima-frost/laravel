<?php


namespace App\Http\Controllers;


use App\Models\Post;
use App\User;

class MyPostsController extends Controller
{
    public function create($user_id, $title, $content){
        $post = new Post();
        $post->user_id = $user_id;
        $post->title = $title;
        $post->content = $content;
        $post->save();

    }
    public function getAllPosts(){
        echo "all posts: <br>";
       $posts = Post::all();
       foreach ($posts as $post){
           echo "<h3>" . $post->title . "</h3> " . $post->content . "<br>";
       };
      }
    public function getOnePost($id){
        $post = Post::where(['id'=>$id])->first();
        echo $post->user_id . " " . $post->title ." " . $post->content;
    }


}
