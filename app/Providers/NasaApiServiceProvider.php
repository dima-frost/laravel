<?php

namespace App\Providers;

use App\Services\NasaApi;
use Illuminate\Support\ServiceProvider;

class NasaApiServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('NasaApi', function ($app){
            return new NasaApi();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
