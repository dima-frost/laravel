<?php


namespace App\Services;

use App\Models\Apod;
use GuzzleHttp\Client;
use Symfony\Component\Validator\Constraints\DateTime;

class NasaApi

{
    protected $date;
    protected $receivedData;
    protected $mediaUrl;
    protected $mediaType;

    public function getData()
    {
        $this->setDate();

        $apod = Apod::where(['date'=>$this->date])->first();
        if(isset($apod->media_url)){
            $this->mediaUrl = $apod->media_url;
            $this->mediaType = $apod->media_type;
        }else {
            $url = "https://api.nasa.gov/planetary/apod?date={$this->date}&api_key=JKahMcScKakEZCN4bCk52IXEe5Ue8w5SKff8njUd";
            $response = (new  Client())->request('GET', $url);
            $body = $response->getBody();
            $receivedData = \GuzzleHttp\json_decode($body, true);

            $this->receivedData = $receivedData;
            $this->mediaUrl = $receivedData['url'];
            $this->mediaType = $receivedData['media_type'];

            $this->create();
        }

    }

    /**
     * @return mixed
     */
    public function getMediaUrl()
    {

        return $this->mediaUrl;
    }

    public function getMediaType()
    {
        return $this->mediaType;
    }

    public function create(){
        $model =new Apod();
        $model->date = $this->receivedData['date'];
        $model->media_type = $this->receivedData['media_type'];
        $model->media_url = $this->receivedData['url'];
        $model->save();
    }

    public function setDate(){

        $nowDay = date('Y-m-d');
        $minDay = "1995-06-16";

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if (isset($_GET['date'])) {
                if ($_GET['date'] <= $nowDay && $_GET['date'] >= $minDay ){
                    $this->date = $_GET['date'];
                }else echo "Вы ввели не верную дату, дата должна быть между " . $minDay . " и " . $nowDay;

            } else {
                $this->date = date('Y-m-d');
            }
        }
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }
}
