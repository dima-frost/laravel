<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', );
});
//Route::get('/post/read/{id}', 'MyPostsController@getOnePost');
Route::get('/post/create/{user_id}&{title}&{content}', 'MyPostsController@create');
Route::get('/posts/read/', 'MyPostsController@getAllPosts');
Route::resource('/post', 'PostsController');
Route::get('/nasa-api', function (){
   $service =  app()->get('NasaApi');
   $service->getData();
   return view('layouts.main', [
       'mediaUrl' => $service->getMediaUrl(),
       'mediaType' => $service->getMediaType(),
       'date'=>$service->getDate(),
   ]);
});




