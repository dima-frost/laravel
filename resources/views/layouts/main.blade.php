<html>
<head>
    <title>Page</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
</head>
<body>
<header class="header">
    <div class="conteiner">
        <!---HEADER--->
    @include('includes.header')
         <!---HEADER--->
    </div>
</header>
<nav class="menu">
   <div class="conteiner">
        <!---DATE--->
      @include('includes.menu')
         <!---DATE--->
   </div>
</nav>
<div class="content_box">
    <div class="content">
        <!---CONTENT--->
        @if($mediaType == 'image')
            <img src="{{$mediaUrl}}">
        @elseif($mediaType == 'video')
        <iframe  width="640" height="480"  src="{{$mediaUrl}}"
                 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        @endif
        <!---CONTENT--->
    </div>
</div>
</body>
<footer>
    <!---FOOTER--->
 @include('includes.footer')
    <!---FOOTER--->
</footer>
</html>
